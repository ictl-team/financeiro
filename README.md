# Financeiro

Este projeto é utilizado para o atendimento do ICTL no que diz respeito a
operações financeiras para os projetos membros. Cada solicitação deve ser feita
através da criação de uma _issue_ neste projeto.

## Orientações gerais

* **Ao criar a sua solicitação, marque a issue como confidencial**, ou sua
  solicitação estará pública na internet (se você e outras partes envolvidas
  não tiverem um problema com isso, nós também não temos).
  * As solicitações confidenciais somente podem ser acessadas pela equipe do
    ICTL e pelos representantes dos projetos membros.
  * Em especial, os representantes dos projetos membros vão poder ver as
    solicitações de outros projetos; contamos com a colaboração de todos
    para que as informações contidas nelas continuem confidenciais. O uso
    impróprio de informações de outros projetos não será tolerado.
* Sempre deixe explícito na sua solicitação a qual projeto membro do ICTL ela
  se refere. Adicione à issue a etiqueta correspondente ao projeto.
* Monitore a issue para receber atualizações.
  * Se necessário, ajuste o nível de notificação para "issues em que eu estou
    participando", ou ligue as notificações especificamente na issue da sua
    solicitação.
* Se você quer falar sobre a solicitação, prestar mais informações, ou tem uma
  pergunta, comente na issue que as pessoas responsáveis serão notificadas. Não
  use email privado.

## Relatórios Financeiros de cada projeto

A contabilidade do ICTL é gerenciada internamente, mas os projetos têm acesso
total à toda a contabilidade relacionadas a eles. Os relatórios são publicados
em arquivos PDF encriptados aqui:

https://ictl-team.pages.debian.net/org/

A senha para os relatórios será informada em privado para os representantes dos
projetos junto ao ICTL.

## Como emitir uma fatura

**Passo 1 - criação da issue**

* Crie uma issue neste projeto:
  * Utilize um título descritivo.
    * Inclua o valor no título da issue pra facilitar a nossa vida na hora de
      saber a qual fatura um determinado pagamento recebido diz respeito.
  * Nos dê detalhes da natureza da operação (doação, patrocínio, qual o evento,
   qual a cota, etc) na descrição da issue.

**Passo 2 - criação da fatura**

* Copie um dos [modelos](modelos/) e preencha todos os dados com atenção:
  * Substitua "NNNN" pelo número da issue que você gerou anteriormente,
    completado com zeros (i.e. `1` vira `0001`)
  * Em "Data" coloque a data da geração do arquivo.
  * Em "Descrição" coloque algo sucinto, como por exemplo: "Cota ouro para o evento-X".
  * Em "Vencimento" coloque o prazo que você achar melhor, ou o que você
    combinou com o patrocinador/doador. Se esse prazo vencer, na prática não há
    problema porque não vamos cobrar juros/multa, então você não precisa gerar
    outro arquivo e enviar para o patrocinador/doador.
* Utilize o número da issue como número da fatura.
* Utilize a seguinte convenção para o nome do arquivo:
  `yyyymmdd-NNNN-nomedaempresa.odt`, onde `yyyymmdd` é a data atual, e `NNNN` é o
  número da fatura (e da issue), completado com zeros (i.e. `1` vira `0001`)
* Após finalizar o preenchimento, converta a fatura para PDF e anexe à issue
  aberta.
* Envie a fatura em PDF para o patrocinador/doador.

**Passo 3 - confirmação do pagamento e fechamento da issue**

* Quando o pagamento entrar na conta-corrente do ICTL, você receberá uma
  notificação na própria issue.
* Não feche a issue. Nós fecharemos quando estivermos fazendo a contabilidade
  do ICTL.

## Como solicitar reembolso de despesas

* Abra uma issue
* Anexe uma nota fiscal que indique claramente o objeto do reembolso.
  Idealmente, a nota fiscal deve estar em nome do ICTL.
* Utilize o corpo da issue para fornecer todos os detalhes que forem relevantes.
* Inclua os dados bancários (banco, conta, agência, CPF) de quem vai receber o
  reembolso desde a solicitação inicial, se isso for possível.
* Lembre-se que o ICTL é uma organização sem fins lucrativos, o que implica que
  o tipo de despesas que nós podemos cobrir precisa estar de acordo com esse
  caráter. Na dúvida, pergunte antes de fazer a despesa.
* Quando o pagamento for feito, você receberá uma notificação na própria issue.
* Não feche a issue. Nós fecharemos quando estivermos fazendo a contabilidade
  do ICTL.

## Como solicitar pagamento de despesas diretamente pelo ICTL

* Abra uma issue
* Utilize o corpo da issue para fornecer todos os detalhes que forem relevantes.
* Anexe
  * um boleto, ou os dados bancários para pagamento por transferência; e
  * um nota fiscal que indique claramente o objeto do pagamento. A nota fiscal
    deve estar em nome do ICTL.
* Lembre-se que o ICTL é uma organização sem fins lucrativos, o que implica que
  o tipo de despesas que nós podemos cobrir precisa estar de acordo com esse
  caráter. Na dúvida, pergunte antes de fazer a despesa.
* Quando o pagamento for feito, você receberá uma notificação na própria issue.
* Não feche a issue. Nós fecharemos quando estivermos fazendo a contabilidade
  do ICTL.

## Como solicitar a assinatura de um contrato

Se for necessário que o ICTL assine um contrato formal com algum apoiador,
patrocinador ou fornecedor do projeto, isto também deve ser solicitado aqui:

* Abra uma issue
* Anexe a minuta do contrato
* Aguarde uma resposta
